import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CatsListService } from './people-cats-list.service';

describe('CatsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [CatsListService]
    });
  });

  it('should create a service', async(inject([CatsListService], (service: CatsListService) => {
    expect(service).toBeTruthy();
  })));

  it('should return the length of cats list is 6 ', async(inject([CatsListService], (service: CatsListService) => {
    service.fetchData().then((data) => {
      expect(data.length).toBe(6); 
    });
  })));

  it('should return an array of people and pets list and the first pet is called Garfield', 
    async(inject([CatsListService], (service: CatsListService) => {
    service.fetchData().then((data) => {
    expect(data[0].pets[0].name).toBe('Garfield');
    });
  })));

  it('should return an array of pets and the first pet type is Cat', 
      async(inject([CatsListService], (service: CatsListService) => {
    service.fetchData().then((data) => {
      expect(data[0].pets[0].type).toBe('Cat');
    });
  })));

  it('should return Null pets with the third people', async(inject([CatsListService], (service: CatsListService) => {
    service.fetchData().then((data) => {
      expect(data[2].pets).toEqual(null);
    });
  })));
});
