import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class CatsListService {
    apiUrl = 'http://agl-developer-test.azurewebsites.net/people.json';

    constructor(private http: HttpClient) {}

    async fetchData() {
        return await this.http.get<any[]>(this.apiUrl).toPromise();        
    }
}


