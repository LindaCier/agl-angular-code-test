import { Component, OnInit } from '@angular/core';
import { CatsListService } from '../services/people-cats-list.service';


@Component({
  selector: 'app-people-cats-list',
  templateUrl: './people-cats-list.component.html',
  styleUrls: ['./people-cats-list.component.less'],
  providers: [CatsListService]
})

export class PeopleCatsListComponent implements OnInit {
  title = 'Cats List';
  
  maleHasCats: any[];
  femaleHasCats: any[];

  constructor(private catsListService: CatsListService) { }

  ngOnInit(): void {
    this.catsListService.fetchData().then(data => { 
      this.maleHasCats = this.filterAndSort(data, 'Male');
      this.femaleHasCats = this.filterAndSort(data, 'Female');
    })
  }

  private filterAndSort(data: any[], gender: string) {
    let filterTemp = [];
    let catsOnlyData = [];
    const filterData = data.filter((x) => x.gender === gender && x.pets !== null)
       
    for (var i = 0; i < filterData.length; i++) {
      filterTemp = filterData[i].pets;
      for (var k = 0; k < filterTemp.length; k++) {
        if (filterTemp[k].type === 'Cat') {
          catsOnlyData.push(filterTemp[k].name);
        }
      }
    }

    catsOnlyData.sort();

    return catsOnlyData;
  }
}
