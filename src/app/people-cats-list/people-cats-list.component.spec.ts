import { TestBed, async, inject } from '@angular/core/testing';
import { CatsListService } from '../services/people-cats-list.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PeopleCatsListComponent } from './people-cats-list.component';

describe('PeopleCatsListComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [
        PeopleCatsListComponent
      ],
      providers: [
        CatsListService
      ],
    });
    TestBed.compileComponents();
  });

  
  it('should create the PeopleCatsListComponent', () => {
    const fixture = TestBed.createComponent(PeopleCatsListComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('should return the list of male-has-cats', async(inject([CatsListService], (service: CatsListService) => {
    const fixture = TestBed.createComponent(PeopleCatsListComponent);
    const component = fixture.debugElement.componentInstance;
    let maleHasCats: any[];
   
    service.fetchData().then((data) => {
          maleHasCats = component.filterAndSort(data, 'Male');
      expect(maleHasCats.length).toEqual(4);
      expect(maleHasCats[0]).toEqual('Garfield');
      expect(maleHasCats[1]).toEqual('Jim');
      expect(maleHasCats[2]).toEqual('Max');
      expect(maleHasCats[3]).toEqual('Tom');
    });
  })));

  it('should return the list of female-has-cats', async(inject([CatsListService], (service: CatsListService) => {
    const fixture = TestBed.createComponent(PeopleCatsListComponent);
    const component = fixture.debugElement.componentInstance;
    let femaleHasCats: any[];
   
    service.fetchData().then((data) => {
          femaleHasCats = component.filterAndSort(data, 'Female');
      expect(femaleHasCats.length).toEqual(4);
      expect(femaleHasCats[0]).toEqual('Garfield');
      expect(femaleHasCats[1]).toEqual('Simba');
      expect(femaleHasCats[2]).toEqual('Tabby');
    });
  })));
});
